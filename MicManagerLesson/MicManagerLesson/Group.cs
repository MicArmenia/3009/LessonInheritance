﻿using System.Collections.Generic;

namespace MicManagerLesson
{
    class Group
    {
        public string Name { get; set; }
        public Teacher Teacher { get; set; }
        public List<Student> Students { get; set; }
    }
}