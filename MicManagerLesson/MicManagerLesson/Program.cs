﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicManagerLesson
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new DataBase();
            List<Teacher> teachers = db.ReadTeachers(3);
            List<Student> students = db.ReadStudents(50);

            var manager = new MicManager();
            List<Group> groups = manager.CreateGroups("C#", teachers, students);

            Print(groups);
            Console.ReadLine();
        }

        static void Print(List<Group> groups)
        {
            //TODO: Implement Print method
        }
    }
}
