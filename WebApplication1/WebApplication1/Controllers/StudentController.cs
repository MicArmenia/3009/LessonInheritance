﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Student>> Get()
        {
            var db = new DataBase();
            return db.ReadStudents();
        }

        // GET api/values/5
        [HttpGet("{count}")]
        public ActionResult<IEnumerable<Student>> Get(int count)
        {
            var db = new DataBase();
            return db.ReadStudents(count);
        }
    }
}