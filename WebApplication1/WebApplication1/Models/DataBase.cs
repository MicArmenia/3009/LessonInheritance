﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class DataBase
    {
        public List<Student> ReadStudents(int count = 10)
        {
            var list = new List<Student>(count);
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var st = new Student
                {
                    Name = $"A{i + 1}",
                    Surname = $"A{i + 1}yan",
                    Age = rnd.Next(17, 25)
                };
                list.Add(st);
            }
            return list;
        }
    }
}
