﻿using System;

namespace Mic.LessonInheritance__Point_3
{
    class Circle : Point
    {
        public Circle()
        {
            r = 5;
        }

        private byte r;
        public byte R
        {
            get { return r; }
            set
            {
                if (value == 0)
                    r = 5;
                else
                    r = value;
            }
        }

        public override void Print()
        {
            base.Print();
            Console.Write($", r = {r}");
        }
    }
}