﻿using System;

namespace Mic.LessonInheritance__Point_3
{
    class Point
    {
        public int x;
        public int y;

        public virtual void Print()
        {
            Console.Write($"({x},{y})");
        }
    }
}