﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__003
{
    class Program
    {
        static void Main(string[] args)
        {
            Person st = new Student
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com",
                age = 18
            };

            var t = new Teacher
            {
                name = "T1",
                surname = "T1yan",
                email = "t1@gmail.com",
                age = 35,
                salary = 5000
            };

            Print(t);
            Print(st);

            Console.ReadLine();
        }

        static void Print(Person source)
        {
            Type type = source.GetType();

            string symbols = new string('*', 5);
            string headerInfo = $"{symbols} {type.Name} {symbols}";
            Console.WriteLine(headerInfo);

            Console.WriteLine(source.FullName);
            Console.WriteLine($"Email: {source.email}");

            //if (source is Teacher)
            //{
            //    Teacher t = (Teacher)source;
            //    Console.WriteLine($"Salary: {t.salary}");
            //}
            var t = source as Teacher;
            if(t != null)
                Console.WriteLine($"Salary: {t.salary}");

            Console.WriteLine(new string('*', headerInfo.Length));
        }
    }
}
