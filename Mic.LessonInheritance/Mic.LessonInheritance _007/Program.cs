﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__007
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Point { x = 10, y = 20 };
            p.Print();
            Console.WriteLine();

            var ci = new Circle { x = 15, y = -20, R = 10 };
            ci.PrintCircle();
            Console.WriteLine();

            var cy = new Cylinder { x = 15, y = -20, R = 10, H = 5};
            cy.PrintCylinder();

            Console.ReadLine();
        }
    }
}
