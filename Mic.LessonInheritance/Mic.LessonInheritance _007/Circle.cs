﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__007
{
    class Circle : Point
    {
        public Circle()
        {
            r = 5;
        }

        private byte r;
        public byte R
        {
            get { return r; }
            set
            {
                if (value == 0)
                    r = 5;
                else
                    r = value;
            }
        }

        public void PrintCircle()
        {
            base.Print();
            Console.Write($", r = {r}");
        }
    }
}