﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__007
{
    class Point
    {
        public int x;
        public int y;

        public void Print()
        {
            Console.Write($"({x},{y})");
        }
    }
}