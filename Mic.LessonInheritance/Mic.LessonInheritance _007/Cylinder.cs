﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__007
{
    class Cylinder : Circle
    {
        public Cylinder()
        {
            h = 5;
        }

        private byte h;
        public byte H
        {
            get { return h; }
            set
            {
                if (value == 0)
                    h = 5;
                else
                    h = value;
            }
        }

        public void PrintCylinder()
        {
            base.PrintCircle();
            Console.Write($", h = {h}");
        }
    }
}