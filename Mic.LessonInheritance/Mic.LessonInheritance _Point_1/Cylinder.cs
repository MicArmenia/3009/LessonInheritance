﻿using System;

namespace Mic.LessonInheritance__Point_1
{
    class Cylinder : Circle
    {
        public Cylinder()
        {
            h = 5;
        }

        private byte h;
        public byte H
        {
            get { return h; }
            set
            {
                if (value == 0)
                    h = 5;
                else
                    h = value;
            }
        }

        public void PrintCylinder()
        {
            base.PrintCircle();
            Console.Write($", h = {h}");
        }
    }
}