﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__Point_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Point { x = 10, y = 20 };
            Print(p);
            //p.Print();
            //Console.WriteLine();

            var ci = new Circle { x = 15, y = -20, R = 10 };
            Print(ci);
            //ci.PrintCircle();
            //Console.WriteLine();

            var cy = new Cylinder { x = 15, y = -20, R = 10, H = 5 };
            Print(cy);
            //cy.PrintCylinder();

            Console.ReadLine();
        }

        static void Print(Point point)
        {
            Type type = point.GetType();

            string symbols = new string('*', 5);
            string headerInfo = $"{symbols} {type.Name} {symbols}";
            Console.WriteLine(headerInfo);

            if (point is Point)
                point.Print();
            else if(point is Circle)
            {
                Circle c = (Circle)point;
                c.PrintCircle();
            }
            else if (point is Cylinder)
            {
                Cylinder c = (Cylinder)point;
                c.PrintCylinder();
            }

            Console.WriteLine();
            Console.WriteLine(new string('*', headerInfo.Length));
        }
        //static void Print(Point point)
        //{
        //    Type type = point.GetType();

        //    string symbols = new string('*', 5);
        //    string headerInfo = $"{symbols} {type.Name} {symbols}";
        //    Console.WriteLine(headerInfo);

        //    point.Print();

        //    Console.WriteLine();
        //    Console.WriteLine(new string('*', headerInfo.Length));
        //}

        //static void Print(Circle point)
        //{
        //    Type type = point.GetType();

        //    string symbols = new string('*', 5);
        //    string headerInfo = $"{symbols} {type.Name} {symbols}";
        //    Console.WriteLine(headerInfo);

        //    point.PrintCircle();

        //    Console.WriteLine();
        //    Console.WriteLine(new string('*', headerInfo.Length));
        //}

        //static void Print(Cylinder point)
        //{
        //    Type type = point.GetType();

        //    string symbols = new string('*', 5);
        //    string headerInfo = $"{symbols} {type.Name} {symbols}";
        //    Console.WriteLine(headerInfo);

        //    point.PrintCylinder();

        //    Console.WriteLine();
        //    Console.WriteLine(new string('*', headerInfo.Length));
        //}
    }
}
