﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var my1 = new MyClass2();

            Console.ReadLine();
        }
    }

    class MyClass1
    {
        public MyClass1()
        {
            Print();
        }

        protected virtual void Print()
        {
            Console.WriteLine("1");
        }
    }

    class MyClass2 : MyClass1
    {
        public MyClass2()
        {
            Print();
        }

        protected override void Print()
        {
            Console.WriteLine("2");
        }
    }
}
