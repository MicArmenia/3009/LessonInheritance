﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__006
{
    class Student
    {
        // ...

        public Student Copy()
        {
            return MemberwiseClone() as Student;
        }
    }

    class Teacher
    {
        // ...

        public Teacher Copy()
        {
            return MemberwiseClone() as Teacher;
        }
    }

    class Group
    {
        public string name;
        public Teacher teacher;
        public List<Student> students;

        public Group Copy()
        {
            var group = MemberwiseClone() as Group;
            group.teacher = this.teacher.Copy();
            group.students = CopyStudents();
            return group;
        }

        private List<Student> CopyStudents()
        {
            var items = new List<Student>(students.Count);
            foreach (Student item in students)
            {
                items.Add(item.Copy());
            }
            return items;
        }
    }
}