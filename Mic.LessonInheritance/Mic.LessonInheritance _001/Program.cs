﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__001
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com",
                age = 18
            };

            var t = new Teacher
            {
                name = "T1",
                surname = "T1yan",
                email = "t1@gmail.com",
                age = 35,
                salary = 5000
            };

            Print(st);
            Print(t);

            Console.ReadLine();
        }

        static void Print(Student source)
        {
            string symbols = new string('*', 5);
            string headerInfo = symbols + "  Student  " + symbols;
            Console.WriteLine(headerInfo);

            Console.WriteLine(source.FullName);
            Console.WriteLine($"Email: {source.email}");

            Console.WriteLine(new string('*', headerInfo.Length));
        }

        static void Print(Teacher source)
        {
            string symbols = new string('*', 5);
            string headerInfo = symbols + "  Teacher  " + symbols;
            Console.WriteLine(headerInfo);

            Console.WriteLine(source.FullName);
            Console.WriteLine($"Email: {source.email}");

            Console.WriteLine(new string('*', headerInfo.Length));
        }
    }
}
