﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__001
{
    class Teacher
    {
        public string name;
        public string surname;
        public string email;
        public byte age;

        public string FullName => $"{name} {surname}";

        public uint salary;
    }
}