﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__004
{
    class Student
    {
        public string name;
        public string surname;
        public string email;
        public byte age;

        public string FullName => $"{name} {surname}";

        //public Student Copy()
        //{
        //    var st = new Student
        //    {
        //        name = this.name,
        //        surname = this.surname,
        //        email = this.email,
        //        age = this.age
        //    };
        //    return st;
        //}

        public Student Copy()
        {
            return MemberwiseClone() as Student;
        }
    }
}