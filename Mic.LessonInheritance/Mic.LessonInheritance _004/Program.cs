﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__004
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com",
                age = 18
            };

            var st1 = st.Copy();
            //var st1 = new Student
            //{
            //    name = st.name,
            //    surname = st.surname,
            //    email = st.email,
            //    age = st.age
            //};
        }
    }
}
