﻿using System;

namespace Mic.LessonInheritance__Point_2
{
    class Cylinder : Circle
    {
        public Cylinder()
        {
            h = 5;
        }

        private byte h;
        public byte H
        {
            get { return h; }
            set
            {
                if (value == 0)
                    h = 5;
                else
                    h = value;
            }
        }

        public new void Print()
        {
            base.Print();
            Console.Write($", h = {h}");
        }
    }
}