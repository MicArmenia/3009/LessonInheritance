﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__Point_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //var p = new Point { x = 10, y = 20 };
            //Print(p);
            ////p.Print();
            ////Console.WriteLine();

            //var ci = new Circle { x = 15, y = -20, R = 10 };
            //Print(ci);
            ////ci.Print();
            ////Console.WriteLine();

            Circle cy = new Cylinder { x = 15, y = -20, R = 10, H = 5 };
            //Print(cy);
            cy.Print();
            Console.WriteLine();

            Console.ReadLine();
        }

        static void Print(Point point)
        {
            Type type = point.GetType();

            string symbols = new string('*', 5);
            string headerInfo = $"{symbols} {type.Name} {symbols}";
            Console.WriteLine(headerInfo);

            point.Print();

            Console.WriteLine();
            Console.WriteLine(new string('*', headerInfo.Length));
        }
    }
}
