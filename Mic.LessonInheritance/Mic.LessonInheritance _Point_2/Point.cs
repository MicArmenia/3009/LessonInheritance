﻿using System;

namespace Mic.LessonInheritance__Point_2
{
    class Point
    {
        public int x;
        public int y;

        public void Print()
        {
            Console.Write($"({x},{y})");
        }
    }
}