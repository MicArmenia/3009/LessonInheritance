﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.LessonInheritance__005
{
    class Person
    {
        public string name;
        public string surname;
        public string email;
        public byte age;

        public string FullName => $"{name} {surname}";

        protected int testField;
    }
}